﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_booking
{
    public partial class DBIForm : Form
    {
        #region disable exit button on this form
        //this code disables the exit button on top right corner for this form
        private const int CP_NoClose_Button = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NoClose_Button;
                return myCp;
            }
        }
        #endregion

        #region Fields
        enum Actions { Add, Alter, Delete }
        enum On { Class, Student }
        private string[] actionDropList = new string[] { "Tilføj", "Rediger", "Fjern" };
        private string[] onDropList = new string[] { "Klasse", "Elev" };
        private int index1, index2;
        private object listItem1, listItem2;
        private List<List<object>> classNames, studentNames, studentNamesInClass, temp = new List<List<object>>();
        #endregion

        public DBIForm()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            HideAll();

            actionDrop1.Items.AddRange(actionDropList);
            ClassUpdateListBox1();
            if (classNames.Count > 0)
            {
                onDrop2.Items.AddRange(onDropList);
            }
            else
            {
                onDrop2.Items.Add(onDropList[0]);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //tilbage
            MenuForm menuForm = new MenuForm();
            menuForm.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            switch (index1)
            {
                case (int)Actions.Add:
                    if (index2 == (int)On.Class)
                    {
                        AddClass();
                    }
                    else
                    {
                        AddStudent();
                    }
                    break;
                case (int)Actions.Alter:
                    if (index2 == (int)On.Class)
                    {
                        AlterClass();
                    }
                    else
                    {
                        AlterStudent();
                    }
                    break;
                case (int)Actions.Delete:
                    if (index2 == (int)On.Class)
                    {
                        DeleteClass();
                    }
                    else
                    {
                        DeleteStudent();
                    }
                    break;
                default:

                    break;
            }
        }

        private void ActionDrop1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ActionDropLogic();
            OnDroplogic();
        }

        private void ActionDropLogic()
        {
            ClearFields();
            index1 = actionDrop1.SelectedIndex;

            if (index2 == (int)On.Class)
            {
                if (index1 == (int)Actions.Add)
                {
                    listBox1.SelectionMode = SelectionMode.None;
                }
                else if (index1 == (int)Actions.Alter)
                {
                    listBox1.SelectionMode = SelectionMode.One;
                }
                else if (index1 == (int)Actions.Delete)
                {
                    listBox1.SelectionMode = SelectionMode.One;
                }
            }
            else if (index2 == (int)On.Student)
            {
                if (index1 == (int)Actions.Add)
                {
                    listBox1.SelectionMode = SelectionMode.One;
                }
                else if (index1 == (int)Actions.Alter)
                {
                    listBox1.SelectionMode = SelectionMode.One;
                }
                else if (index1 == (int)Actions.Delete)
                {
                    listBox1.SelectionMode = SelectionMode.One;
                    listBox2.SelectionMode = SelectionMode.One;
                }
            }
        }

        private void OnDrop2_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnDroplogic();
            ActionDropLogic();
        }

        private void OnDroplogic()
        {
            ClearFields();
            HideAll();
            index2 = onDrop2.SelectedIndex;
            if (index2 == (int)On.Class)
            {
                #region Class
                if (index1 == (int)Actions.Add)
                {
                    ListLabel.Text = "Klasser";
                    ListLabel.Show();
                    ClassUpdateListBox1();
                    listBox1.Show();

                    nameLabel.Text = "*Klasse navn";
                    nameLabel.Show();
                    nameBox.Show();
                }
                else if (index1 == (int)Actions.Alter)
                {
                    ListLabel.Text = "Klasser";
                    ListLabel.Show();
                    ClassUpdateListBox1();
                    listBox1.Show();

                    nameLabel.Text = "Klasse navn";
                    nameLabel.Show();
                    nameBox.Show();

                }
                else if (index1 == (int)Actions.Delete)
                {
                    ListLabel.Text = "Klasser";
                    ListLabel.Show();
                    ClassUpdateListBox1();
                    listBox1.Show();
                }
                #endregion
            }
            else if (index2 == (int)On.Student)
            {
                #region Student
                if (index1 == (int)Actions.Add)
                {
                    nameLabel.Text = "*Navn:";
                    nameLabel.Show();
                    nameBox.Show();

                    studentMailLabel.Show();
                    mailBox.Show();

                    fmailLabel.Show();
                    FMailBox.Show();

                    passLabel.Show();
                    passBox.Show();

                    ListLabel.Text = "Klasser:";
                    ClassUpdateListBox1();
                    ListLabel.Show();
                    listBox1.Show();
                }
                else if (index1 == (int)Actions.Alter)
                {
                    nameLabel.Text = "Navn:";
                    nameLabel.Show();
                    nameBox.Show();

                    studentMailLabel.Show();
                    mailBox.Show();

                    fmailLabel.Show();
                    FMailBox.Show();

                    passLabel.Show();
                    passBox.Show();

                    ListLabel.Text = "Elever:";
                    ListLabel.Show();
                    StudentUpdateListBox1();
                    listBox1.Show();
                }
                else if (index1 == (int)Actions.Delete)
                {
                    ListLabel.Text = "Klasser:";
                    ListLabel.Show();
                    ClassUpdateListBox1();
                    listBox1.Show();

                    listbox2Label.Show();
                    listBox2.Show();
                }
                #endregion
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listItem1 = listBox1.SelectedItem;
            temp.Clear();
            if (index2 == (int)On.Class)
            {
                if (index1 == (int)Actions.Alter)
                {
                    foreach (List<object> item in classNames)
                    {
                        if ((string)item[1] == (string)listItem1)
                        {
                            nameBox.Text = (string)item[1];
                            temp.Add(item);
                        }
                    }

                }
                else if (index1 == (int)Actions.Delete)
                {
                    foreach (List<object> item in classNames)
                    {
                        if ((string)item[1] == (string)listItem1)
                        {
                            temp.Add(item);
                        }
                    }
                }
            }
            else if (index2 == (int)On.Student)
            {
                if (index1 == (int)Actions.Add)
                {
                    foreach (List<object> item in classNames)
                    {
                        if ((string)item[1] == (string)listItem1)
                        {
                            temp.Add(item);
                        }
                    }
                }
                else if (index1 == (int)Actions.Alter) //almost done
                {
                    foreach (List<object> item in studentNames)
                    {
                        if ((string)item[1] == (string)listItem1)
                        {
                            ClassUpdateListBox1();
                            nameBox.Text = (string)item[1];
                            FMailBox.Text = (string)item[2];
                            if (item[3] != System.DBNull.Value)
                            {
                                mailBox.Text = (string)item[3];
                            }
                            passBox.Text = (string)item[4];
                        }
                    }

                }
                else if (index1 == (int)Actions.Delete)
                {
                    foreach (List<object> item in classNames)
                    {
                        if ((string)item[1] == (string)listItem1)
                        {
                            temp.Add(item);
                        }
                    }
                    listbox2Label.Show();
                    StudentUpdateListBox2();
                    temp.Clear();
                    listBox2.Show();
                }
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            listItem2 = listBox2.SelectedItem;
            temp.Clear();
            if (index2 == (int)On.Class)
            {
                if (index1 == (int)Actions.Alter)
                {

                }
                else if (index1 == (int)Actions.Delete)
                {

                }
            }
            else if (index2 == (int)On.Student)
            {
                if (index1 == (int)Actions.Add)
                {

                }
                else if (index1 == (int)Actions.Alter)
                {

                }
                else if (index1 == (int)Actions.Delete)
                {
                    foreach (List<object> item in studentNamesInClass)
                    {
                        if (item[1].ToString().ToLower() == (string)listItem2)
                        {
                            temp.Add(item);
                        }
                    }
                }
            }
        }

        private void ClassUpdateListBox1()
        {
            listBox1.Items.Clear();

            classNames = SQL.Instance.SelectClasses();

            foreach (List<object> item in classNames)
            {
                listBox1.Items.Add(item[1]);
            }
        }

        private void StudentUpdateListBox1()
        {
            listBox1.Items.Clear();

            studentNames = SQL.Instance.SelectStudents();

            foreach (List<object> item in studentNames)
            {
                listBox1.Items.Add(item[1]);
            }
        }

        private void StudentUpdateListBox2()
        {
            int classid = 0;

            foreach (List<object> item in temp)
            {
                classid = Int32.Parse(item[0].ToString());
            }
            studentNamesInClass = SQL.Instance.SelectStudentsPerClass(classid);

            listBox2.Items.Clear();
            foreach (List<object> item in studentNamesInClass)
            {
                listBox2.Items.Add(item[1]);
            }
            temp.Clear();
        }

        private void AddClass()
        {
            SQL.Instance.AddClass(nameBox.Text);
            ClassUpdateListBox1();

            onDrop2.Items.Clear();
            if (classNames.Count > 0 && onDrop2.Items.Count < 2)
            {
                onDrop2.Items.AddRange(onDropList);
            }
        }

        private void AlterClass()
        {
            int classid = 0;
            string className = "";
            foreach (List<object> item in temp)
            {
                classid = Int32.Parse(item[0].ToString());
                className = nameBox.Text.ToLower();
            }
            if (className != "" && classid != 0)
            {
                SQL.Instance.AlterClass(className, classid);
                ClassUpdateListBox1();
            }
            temp.Clear();
        } //Done

        private void DeleteClass()
        {
            string className = "";
            foreach (List<object> item in temp)
            {
                className = (string)item[1];
            }
            SQL.Instance.DeleteClass(className);
            ClassUpdateListBox1();
            temp.Clear();
        }

        private void AddStudent()
        {
            int classid = 0;
            string className = "";
            foreach (List<object> item in temp)
            {
                classid = (1 + Int32.Parse(item[0].ToString()));
                className = (string)item[1];
            }

            if (mailBox.Text != "")
            {
                SQL.Instance.AddStudent(nameBox.Text, FMailBox.Text, classid, passBox.Text, mailBox.Text);
            }
            else
            {
                SQL.Instance.AddStudent(nameBox.Text, FMailBox.Text, classid, passBox.Text);
            }
            
            ClassUpdateListBox1();
            listBox1.Show();
            StudentUpdateListBox2();
            listbox2Label.Text = className;
            temp.Clear();
            ClearFields();
        }

        private void AlterStudent() //not done 
        {

            //SQL.Instance.AlterStudent(userid, name, fmail, classid, mail);
        }

        private void DeleteStudent() //almost done
        {
            string name = "";
            foreach (List<object> item in temp)
            {
                name = (string)item[1];
            }
            SQL.Instance.DeleteStudent(name);
            StudentUpdateListBox2();
            temp.Clear();
        }

        private void HideAll()
        {
            nameLabel.Hide();
            nameBox.Hide();

            studentMailLabel.Hide();
            mailBox.Hide();

            fmailLabel.Hide();
            FMailBox.Hide();

            passLabel.Hide();
            passBox.Hide();

            listbox2Label.Hide();
            listBox2.Hide();
        }

        private void ClearFields()
        {
            nameBox.Text = "";
            mailBox.Text = "";
            FMailBox.Text = "";
            passBox.Text = "";

        }
    }
}
