﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_booking
{
    public partial class UserMenuForm : Form
    {
        #region disable exit button on this form
        //this code disables the exit button on top right corner for this form
        private const int CP_NoClose_Button = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NoClose_Button;
                return myCp;
            }
        }
        #endregion

        public UserMenuForm()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            //(username label)
        }

        public List<object> users = new List<object>();

        private void monthCalendar1_DateChanged_1(object sender, DateRangeEventArgs e)
        {
            //kallender
            if (monthCalendar1.BoldedDates.Contains(monthCalendar1.SelectionStart))
            {
                FuldBooket frm = new FuldBooket();
                frm.dato = monthCalendar1.SelectionStart.ToString();
                //giver listen videre
                frm.userList = users;
                frm.Show();
                this.Close();
            }
            else
            {
                Booked frm = new Booked();
                frm.dato = monthCalendar1.SelectionStart.ToString();
                //giver listen videre
                frm.userList = users;
                frm.Show();
                this.Close();
            }

        }
    }
}
