﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_booking
{
    public partial class LoginForm : Form
    {
        List<List<object>> input = new List<List<object>>();

        public LoginForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label3.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.ToLower() == textBox1.Text.ToLower() && textBox1.Text.ToLower() == "admin")//admin login
            {
                MenuForm frm4 = new MenuForm();//admin interface
                frm4.Show();
                this.Hide();
            }
            else if (textBox1.Text.ToLower() == "" && textBox2.Text.ToLower() == "") //Missing input...
            {
                //Failure to login logic happens here...
                label3.Show();
            }
            else //normal login
            {
                input = SQL.Instance.SelectLoginFromUsers(textBox1.Text.ToLower(), textBox2.Text.ToLower());
                if (input.Count > 0)
                {
                    if (textBox1.Text.ToLower() == input[1].ToString() && textBox2.Text.ToLower() == input[2].ToString())
                    {
                        UserMenuForm UserMForm = new UserMenuForm();//userinterface WIP
                        UserMForm.Show();
                        UserMForm.Text = input[0].ToString();
                        this.Hide();
                    }
                }
                else
                {
                    //Failure to login logic happens here....
                    textBox1.Text = "";
                    textBox2.Text = "";
                    label3.Show();
                }
                

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
