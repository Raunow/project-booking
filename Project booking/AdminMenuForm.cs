﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_booking
{
    public partial class MenuForm : Form
    {
        #region disable exit button on this form
        //this code disables the exit button on top right corner for this form
        private const int CP_NoClose_Button = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NoClose_Button;
                return myCp;
            }
        }
        #endregion

        public MenuForm()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //log af
            LoginForm loginForm = new LoginForm();
            loginForm.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //planlæg session
            PlannerForm plannerForm = new PlannerForm();
            plannerForm.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //ændre elev oplysninger
            DBIForm DBIForm = new DBIForm();
            DBIForm.Show();
            this.Close();
        }
    }
}
