﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_booking
{
    public partial class PlannerForm : Form
    {
        #region disable exit button on this form
        //this code disables the exit button on top right corner for this form
        private const int CP_NoClose_Button = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NoClose_Button;
                return myCp;
            }
        }
        #endregion

        bool mandage = false, tirsdage = false, onsdage = false, torsdage = false, fredage = false, lørdage = false, søndage = false;
        int date = 0, month = 1;
        List<DateTime> dato = new List<DateTime>();
        private List<int> dayState, temp = new List<int>();

        public PlannerForm()
        {
            InitializeComponent();
        }

        private void PlannerForm_Load(object sender, EventArgs e)
        {
            UpdateCHeckedListBox1();
            CreateDateList();
            BoldDates();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //tilbage
            MenuForm menuForm = new MenuForm();
            menuForm.Show();
            this.Close();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            //booked af textbox
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //gem indstillinger
            #region Checkbox week days
            if (checkedListBox1.SelectedItems.Count > 0)
            {
                int i = 0;
                foreach (string item in checkedListBox1.CheckedItems)
                {
                    i++;
                    temp.Add(0);
                    switch (item.ToString())
                    {
                        case "Mandage":
                            temp[0] = 1;
                            mandage = true;
                            break;
                        case "Tirsdage":
                            temp[1] = 1;
                            tirsdage = true;
                            break;
                        case "Onsdage":
                            temp[2] = 1;
                            onsdage = true;
                            break;
                        case "Torsdage":
                            temp[3] = 1;
                            torsdage = true;
                            break;
                        case "Fredage":
                            temp[4] = 1;
                            fredage = true;
                            break;
                        case "Lørdage":
                            temp[5] = 1;
                            lørdage = true;
                            break;
                        case "Søndage":
                            temp[6] = 1;
                            søndage = true;
                            break;
                        default:
                            temp[i] = 0;
                            mandage = false;
                            tirsdage = false;
                            onsdage = false;
                            torsdage = false;
                            fredage = false;
                            lørdage = false;
                            søndage = false;
                            break;
                    }
                }
                dayState = temp;
                UpdateDatabase();
            }
            #endregion
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //mangler dage
            MSForm MSForm = new MSForm();
            MSForm.Show();
            this.Close();
        }

        private void UpdateDatabase()
        {
            //idk if i did this right .. o-o' oh well.. do magic here.
        }

        private void UpdateCHeckedListBox1()
        {
            dayState = SQL.Instance.selectFromDays();
            int i = 0;
            foreach (int item in dayState)
            {
                checkedListBox1.SetItemChecked(i, item == 1 ? true : false);
                i++;
            }
        }

        private void CreateDateList()
        {
            for (int i = 0; i < 365; i++)
            {
                date++;
                dato.Add(new DateTime(2017, month, date));
                if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
                {
                    if (date == 31)
                    {
                        date = 0;
                        if (month != 12)
                        {
                            month++;
                        }
                        else
                        {
                            month = 1;
                        }
                    }
                }
                else if (date == 28 && month == 2)
                {
                    date = 0;
                    month++;
                }
                else if (month == 4 || month == 6 || month == 9 || month == 11)
                {
                    if (date == 30)
                    {
                        date = 0;
                        month++;
                    }
                }
            }
        }

        private void BoldDates()
        {
            foreach (var item in dato)
            {
                monthCalendar1.AddBoldedDate(item);
            }
            monthCalendar1.UpdateBoldedDates();
        }

        private void RemoveBoldedDates()
        {
            foreach (DateTime item in dato)
            {
                monthCalendar1.RemoveBoldedDate(item);
                switch (item.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        if (!mandage)
                            monthCalendar1.AddBoldedDate(item);
                        break;
                    case DayOfWeek.Tuesday:
                        if (!tirsdage)
                            monthCalendar1.AddBoldedDate(item);
                        break;
                    case DayOfWeek.Wednesday:
                        if (!onsdage)
                            monthCalendar1.AddBoldedDate(item);
                        break;
                    case DayOfWeek.Thursday:
                        if (!torsdage)
                            monthCalendar1.AddBoldedDate(item);
                        break;
                    case DayOfWeek.Friday:
                        if (!fredage)
                            monthCalendar1.AddBoldedDate(item);
                        break;
                    case DayOfWeek.Saturday:
                        if (!lørdage)
                            monthCalendar1.AddBoldedDate(item);
                        break;
                    case DayOfWeek.Sunday:
                        if (!søndage)
                            monthCalendar1.AddBoldedDate(item);
                        break;
                    default:
                        break;
                }
                monthCalendar1.UpdateBoldedDates();
            }
        }

        private void SaveSessions()
        {
            byte maksDeltagelseDag = (byte)numericUpDown2.Value;
            byte minDeltagelseMåned = (byte)numericUpDown1.Value;
            //SQL.Instance.SaveSessions();
        }
    }
}
