﻿namespace Project_booking
{
    partial class DBIForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.actionDrop1 = new System.Windows.Forms.ComboBox();
            this.onDrop2 = new System.Windows.Forms.ComboBox();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.studentMailLabel = new System.Windows.Forms.Label();
            this.mailBox = new System.Windows.Forms.TextBox();
            this.fmailLabel = new System.Windows.Forms.Label();
            this.FMailBox = new System.Windows.Forms.TextBox();
            this.passLabel = new System.Windows.Forms.Label();
            this.passBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.actionLabel = new System.Windows.Forms.Label();
            this.onLabel = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.ListLabel = new System.Windows.Forms.Label();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listbox2Label = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // actionDrop1
            // 
            this.actionDrop1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.actionDrop1.FormattingEnabled = true;
            this.actionDrop1.Location = new System.Drawing.Point(27, 63);
            this.actionDrop1.Name = "actionDrop1";
            this.actionDrop1.Size = new System.Drawing.Size(120, 21);
            this.actionDrop1.TabIndex = 1;
            this.toolTip1.SetToolTip(this.actionDrop1, "Vælg handling");
            this.actionDrop1.SelectedIndexChanged += new System.EventHandler(this.ActionDrop1_SelectedIndexChanged);
            // 
            // onDrop2
            // 
            this.onDrop2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.onDrop2.FormattingEnabled = true;
            this.onDrop2.Location = new System.Drawing.Point(27, 121);
            this.onDrop2.Name = "onDrop2";
            this.onDrop2.Size = new System.Drawing.Size(120, 21);
            this.onDrop2.TabIndex = 2;
            this.toolTip1.SetToolTip(this.onDrop2, "Vælg");
            this.onDrop2.SelectedIndexChanged += new System.EventHandler(this.OnDrop2_SelectedIndexChanged);
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(208, 64);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(210, 20);
            this.nameBox.TabIndex = 4;
            this.toolTip1.SetToolTip(this.nameBox, "Navn");
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(205, 45);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(40, 16);
            this.nameLabel.TabIndex = 5;
            this.nameLabel.Text = "Navn";
            // 
            // studentMailLabel
            // 
            this.studentMailLabel.AutoSize = true;
            this.studentMailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentMailLabel.Location = new System.Drawing.Point(205, 105);
            this.studentMailLabel.Name = "studentMailLabel";
            this.studentMailLabel.Size = new System.Drawing.Size(36, 16);
            this.studentMailLabel.TabIndex = 7;
            this.studentMailLabel.Text = "Mail:";
            // 
            // mailBox
            // 
            this.mailBox.Location = new System.Drawing.Point(208, 124);
            this.mailBox.Name = "mailBox";
            this.mailBox.Size = new System.Drawing.Size(210, 20);
            this.mailBox.TabIndex = 5;
            this.toolTip1.SetToolTip(this.mailBox, "Mail");
            // 
            // fmailLabel
            // 
            this.fmailLabel.AutoSize = true;
            this.fmailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fmailLabel.Location = new System.Drawing.Point(205, 165);
            this.fmailLabel.Name = "fmailLabel";
            this.fmailLabel.Size = new System.Drawing.Size(100, 16);
            this.fmailLabel.TabIndex = 9;
            this.fmailLabel.Text = "*Forældre Mail:";
            // 
            // FMailBox
            // 
            this.FMailBox.Location = new System.Drawing.Point(208, 184);
            this.FMailBox.Name = "FMailBox";
            this.FMailBox.Size = new System.Drawing.Size(210, 20);
            this.FMailBox.TabIndex = 6;
            // 
            // passLabel
            // 
            this.passLabel.AutoSize = true;
            this.passLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passLabel.Location = new System.Drawing.Point(205, 225);
            this.passLabel.Name = "passLabel";
            this.passLabel.Size = new System.Drawing.Size(48, 16);
            this.passLabel.TabIndex = 11;
            this.passLabel.Text = "*Kode:";
            // 
            // passBox
            // 
            this.passBox.Location = new System.Drawing.Point(208, 244);
            this.passBox.Name = "passBox";
            this.passBox.Size = new System.Drawing.Size(210, 20);
            this.passBox.TabIndex = 7;
            this.toolTip1.SetToolTip(this.passBox, "Password\r\n");
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(605, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 23);
            this.button1.TabIndex = 10;
            this.button1.TabStop = false;
            this.button1.Text = "Tilbage";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // actionLabel
            // 
            this.actionLabel.AutoSize = true;
            this.actionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.actionLabel.Location = new System.Drawing.Point(24, 45);
            this.actionLabel.Name = "actionLabel";
            this.actionLabel.Size = new System.Drawing.Size(62, 16);
            this.actionLabel.TabIndex = 14;
            this.actionLabel.Text = "Handling";
            // 
            // onLabel
            // 
            this.onLabel.AutoSize = true;
            this.onLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.onLabel.Location = new System.Drawing.Point(24, 102);
            this.onLabel.Name = "onLabel";
            this.onLabel.Size = new System.Drawing.Size(41, 16);
            this.onLabel.TabIndex = 15;
            this.onLabel.Text = "Vælg";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(528, 397);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(136, 33);
            this.button2.TabIndex = 9;
            this.button2.Text = "Udfør";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(27, 182);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 199);
            this.listBox1.TabIndex = 3;
            this.toolTip1.SetToolTip(this.listBox1, "Klasse oversigt");
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // ListLabel
            // 
            this.ListLabel.AutoSize = true;
            this.ListLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListLabel.Location = new System.Drawing.Point(24, 163);
            this.ListLabel.Name = "ListLabel";
            this.ListLabel.Size = new System.Drawing.Size(56, 16);
            this.ListLabel.TabIndex = 16;
            this.ListLabel.Text = "Klasser:";
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(153, 182);
            this.listBox2.Name = "listBox2";
            this.listBox2.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listBox2.Size = new System.Drawing.Size(120, 199);
            this.listBox2.TabIndex = 21;
            this.listBox2.TabStop = false;
            this.toolTip1.SetToolTip(this.listBox2, "Elev oversigt");
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // listbox2Label
            // 
            this.listbox2Label.AutoSize = true;
            this.listbox2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listbox2Label.Location = new System.Drawing.Point(150, 163);
            this.listbox2Label.Name = "listbox2Label";
            this.listbox2Label.Size = new System.Drawing.Size(47, 16);
            this.listbox2Label.TabIndex = 20;
            this.listbox2Label.Text = "Elever";
            // 
            // DBIForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 441);
            this.Controls.Add(this.listbox2Label);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ListLabel);
            this.Controls.Add(this.onLabel);
            this.Controls.Add(this.actionLabel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.passLabel);
            this.Controls.Add(this.passBox);
            this.Controls.Add(this.fmailLabel);
            this.Controls.Add(this.FMailBox);
            this.Controls.Add(this.studentMailLabel);
            this.Controls.Add(this.mailBox);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.onDrop2);
            this.Controls.Add(this.actionDrop1);
            this.Controls.Add(this.listBox2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DBIForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox actionDrop1;
        private System.Windows.Forms.ComboBox onDrop2;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label studentMailLabel;
        private System.Windows.Forms.TextBox mailBox;
        private System.Windows.Forms.Label fmailLabel;
        private System.Windows.Forms.TextBox FMailBox;
        private System.Windows.Forms.Label passLabel;
        private System.Windows.Forms.TextBox passBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label actionLabel;
        private System.Windows.Forms.Label onLabel;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label ListLabel;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label listbox2Label;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}