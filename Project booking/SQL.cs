﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;

namespace Project_booking
{
    public class SQL
    {
        #region Fields
        private static SQL instance;
        SQLiteConnection conn;
        SQLiteCommand cmd;
        SQLiteDataReader reader;
        #endregion

        #region Properties
        public static SQL Instance
        {
            get
            {
                if (instance == null)
                    instance = new SQL();
                return instance;
            }
        }

        #endregion

        private SQL()
        {
            conn = new SQLiteConnection("Data Source = register.db; Version = 3;");
            conn.Open();

            Initialise();
        }

        #region Methods
        private void Write(string str, List<SQLiteParameter> param = null)
        {
            cmd = new SQLiteCommand(str, conn);
            #region SQL-injection protection
            if (param != null)
            {
                foreach (var p in param)
                {
                    cmd.Parameters.Add(p);
                }
            }
            #endregion
            cmd.ExecuteNonQuery();
        }

        private List<List<object>> Read(string str, List<string> toRead = null, List<SQLiteParameter> param = null)
        {
            int columns = 0;
            List<List<object>> output = new List<List<object>>();
            List<object> fill = new List<object>();
            cmd = new SQLiteCommand(str, conn);
            #region SQL-injection protection
            if (param != null)
            {
                foreach (var p in param)
                {
                    cmd.Parameters.Add(p);
                }
            }
            #endregion

            try
            {
                if (toRead != null)
                {
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        for (int i = 0; i < toRead.Count; i++)
                        {
                            fill.Add(reader["" + toRead[i]]);
                        }
                        output.Add(fill);
                        fill = new List<object>();
                    }
                }
                else
                {
                    reader = cmd.ExecuteReader();
                    columns = reader.FieldCount;

                    while (reader.Read())
                    {
                        for (int i = 0; i < columns; i++)
                        {
                            fill.Add(reader[i]);
                        }
                        output.Add(fill);
                        fill = new List<object>();
                    }
                }
            }
            catch
            {
                output = null;
            }

            return output;
        }

        private int WriteScalar(string str, List<SQLiteParameter> param = null)
        {
            cmd = new SQLiteCommand(str, conn);
            #region SQL-injection protection
            if (param != null)
            {
                foreach (var p in param)
                {
                    cmd.Parameters.Add(p);
                }
            }
            #endregion

            return Convert.ToInt32(cmd.ExecuteScalar());
        }
        #endregion

        #region program dependent methods
        private void Initialise()
        {
            Write("CREATE TABLE IF NOT EXISTS users (userid INTEGER PRIMARY KEY, name string NOT NULL, fmail string NOT NULL, mail string, pass string NOT NULL, sessions INTEGER NOT NULL, 'class' INTEGER, FOREIGN KEY('class') REFERENCES classes(classid));");
            Write("CREATE TABLE IF NOT EXISTS classes(classid INTEGER PRIMARY KEY, name string NOT NULL);");
            Write("CREATE TABLE IF NOT EXISTS days ('Mandag' INT, 'Tirsdag' INT, 'Onsdag' INT, 'Torsdag' INT, 'Fredag' INT, 'Lørdag' INT, 'Søndag' INT);");
            
            if (WriteScalar("SELECT Count(*) FROM days") == 0)
            {
                Write("INSERT INTO days values (0, 0, 0, 0, 0, 0, 0);");
            }
        }

        public List<int> selectFromDays()
        {
            List<int> days = new List<int>();
            List<List<object>> temp = new List<List<object>>();

            string str = "SELECT * FROM days;";
            temp = Read(str);
            foreach (List<object> arr in temp)
            {
                foreach (int item in arr)
                {
                    days.Add(item);
                }
            }

            return days;
        }

        public void SaveToDays(List<SQLiteParameter> param)
        {
            List<SQLiteParameter> tempParam = new List<SQLiteParameter>();
            
            string str = "UPDATE days SET status = @status where name = @name;";
            
            foreach (var p in param)
            {
                tempParam.Add(p);
                Write(str, tempParam);
                tempParam.Remove(p);
            }
        }

        public List<List<object>> SelectClasses()
        {
            string str = "SELECT * FROM classes ORDER BY name ASC";
            
            return Read(str); ;
        }

        public List<List<object>> SelectStudents()
        {
            string str = "SELECT * FROM users ORDER BY name ASC";
            
            return Read(str); ;
        }

        public List<List<object>> SelectStudentsPerClass(int classid)
        {
            List<SQLiteParameter> param = new List<SQLiteParameter>();
                param.Add(new SQLiteParameter("class", classid));
            List<string> toRead = new List<string>();
                toRead.Add("userid");
                toRead.Add("name");
                toRead.Add("class");

            string str = "SELECT userid,name,class FROM users WHERE class = @class ORDER BY name ASC";
            
            return Read(str, toRead, param); ;
        }

        public List<List<object>> SelectLoginFromUsers(string mail, string pass)
        {
            List<SQLiteParameter> param = new List<SQLiteParameter>();
                param.Add(new SQLiteParameter("mail", mail));
                param.Add(new SQLiteParameter("pass", pass));
            List<string> toRead = new List<string>();
                toRead.Add("name");
                toRead.Add("fmail");
                toRead.Add("pass");

            string str = "SELECT name,fmail,pass FROM users WHERE fmail = @mail and pass = @pass;";

            return Read(str, toRead, param);
        }

        public List<object> SelectNameSessionFromUsers()
        {
            List<object> user = new List<object>();
            List<List<object>> temp = new List<List<object>>();
            List<string> toRead = new List<string>();
                toRead.Add("name");
                toRead.Add("sessions");

            string str = "SELECT name,sessions FROM users;";

            temp = Read(str, toRead);
            if (temp != null)
            {
                foreach (List<object> item in temp)
                {
                    user.Add(item);
                }
            }
            else
            {
                user.Add(null);
            }
            return (user);
        }

        public void AddClass(string name)
        {
            List<SQLiteParameter> param = new List<SQLiteParameter>();
                param.Add(new SQLiteParameter("name", name));

            string str = "INSERT INTO classes VALUES (null, @name)";

            Write(str, param);
        }

        public void AlterClass(string name, int classid)
        {
            List<SQLiteParameter> param = new List<SQLiteParameter>();
                param.Add(new SQLiteParameter("name", name));
                param.Add(new SQLiteParameter("classid", classid));

            string str = "UPDATE classes SET name = @name WHERE classid = @classid";

            Write(str, param);
        }

        public void DeleteClass(string className)
        {
            List<SQLiteParameter> param = new List<SQLiteParameter>();
                param.Add(new SQLiteParameter("name", className));

            string str = "DELETE FROM classes WHERE name = @name;";

            Write(str, param);
        }

        public void AddStudent(string name, string fmail, int classid, string pass, string mail = null)
        {
            List<SQLiteParameter> param = new List<SQLiteParameter>();
                param.Add(new SQLiteParameter("name", name));
                param.Add(new SQLiteParameter("fmail", fmail));
                param.Add(new SQLiteParameter("classid", classid));
                param.Add(new SQLiteParameter("pass", pass));
                param.Add(new SQLiteParameter("mail", mail));

            string str = "INSERT INTO users (name, fmail, mail, pass, sessions, 'class') VALUES (@name, @fmail, @mail, @pass, 0, @classid);";

            Write(str, param);
        }

        public void AlterStudent(int userid, List<string[]> alteration)
        {
            
            List<SQLiteParameter> param = new List<SQLiteParameter>();
            
            foreach (string[] item in alteration)
            {
                param.Add(new SQLiteParameter("userid", userid));
                param.Add(new SQLiteParameter("field", item[1]));
                string str = "UPDATE users SET '" + item[0] + "' = @field WHERE userid = @userid";
                Write(str, param);
                param.Clear();
            }
        }

        public void DeleteStudent(string name)
        {
            List<SQLiteParameter> param = new List<SQLiteParameter>();
                param.Add(new SQLiteParameter("name", name));
            
            string str = "DELETE FROM users WHERE name = @name";

            Write(str, param);
        }
        #endregion region
    }
}